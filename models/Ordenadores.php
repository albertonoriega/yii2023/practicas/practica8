<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ordenadores".
 *
 * @property int $id
 * @property string|null $descripcion
 * @property string|null $procesador
 * @property string|null $memoria
 * @property string|null $discoDuro
 * @property int|null $ethernet
 * @property int|null $wifi
 * @property string|null $tarjetaVideo
 */
class Ordenadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ordenadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ethernet', 'wifi'], 'boolean'],
            [['descripcion'], 'string', 'max' => 255],
            [['procesador', 'memoria', 'discoDuro', 'tarjetaVideo'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'descripcion' => 'Descripción',
            'procesador' => 'Procesador',
            'memoria' => 'Memoria',
            'discoDuro' => 'Disco duro',
            'ethernet' => 'Tarjeta de red Ethernet',
            'wifi' => 'Tarjeta de red Wifi',
            'tarjetaVideo' => 'Tarjeta Video',
        ];
    }
}
