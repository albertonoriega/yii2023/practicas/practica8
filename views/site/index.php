<?php

/** @var yii\web\View $this */

use yii\helpers\Html;

$this->title = 'My Yii Application';
?>


<div class="body-content">

    <div class="row mt-5">
        <div class="col-lg-4 ">

            <?= Html::img('@web/imgs/ordenadorIndex.jpg', ['class' => 'imagenIndex']) ?>
        </div>
        <div class="col-lg-8 ">
            <h1>Aplicación de ordenadores</h1>
            <div class="row mt-5">
                <div class="col-lg-6">
                    <p> En <span>Administrar</span> podremos ver el listado de todos los ordenadores, está hecho de dos formas:</p>
                    <ul>
                        <li>La primera forma, está hecha haciendo una tabla a mano iterando los registros con un foreach</li>
                        <li>La segunda forma, se realiza con el GridView de yii</li>
                    </ul>
                </div>
                <div class="col-lg-6">
                    <p>En listar podremos ver cada registro en una card en la que si le damos al boton vemos todos los detalles del registro</p>
                </div>
            </div>

        </div>

    </div>

</div>
</div>