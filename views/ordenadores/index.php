<?php

use yii\helpers\Html;

$this->title = 'Ordenadores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ordenadores-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    $modelo = ($dataProvider->getModels());


    ?>
    <table class="tablaGrid">
        <thead>
            <tr>
                <?php
                foreach ($modelo[0] as $key => $value) {
                ?>
                    <td> <?= ucfirst($key) ?></td>
                <?php
                }
                ?>
                <td></td>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($modelo as $dato) {
            ?>

                <tr>
                    <td><?= $dato['id'] ?></td>
                    <td><?= $dato['descripcion'] ?></td>
                    <td><?= $dato['procesador'] ?></td>
                    <td><?= $dato['memoria'] ?></td>
                    <td><?= $dato['discoDuro'] ?></td>
                    <td><?= $dato['ethernet'] ? '<i class="fas fa-check-square"></i>' : '<i class="far fa-square"></i>' ?></td>
                    <td><?= $dato['wifi'] ? '<i class="fas fa-check-square"></i>' : '<i class="far fa-square"></i>' ?></td>
                    <td><?= $dato['tarjetaVideo'] ?></td>
                    <td>
                        <?= Html::a('<span class="spanIconoVer"><i class="fas fa-eye"></i></span>', ['view', 'id' => $dato['id']], ['class' => 'icono iconoVer']) ?>
                        <?= Html::a('<i class="fas fa-pencil-alt"></i>', ['update', 'id' => $dato['id']], ['class' => 'icono iconoUpdate']) ?>
                        <?= Html::a('<i class="fas fa-trash-alt"></i>', ['/ordenadores/delete', 'id' => $dato['id']], ['class' => 'icono iconoDelete', 'data' => [
                            'confirm' => '¿Estás seguro que quieres borrar el regsitro?',
                            'method' => 'post',
                        ],]) ?>
                        <?= Html::a('<i class="fas fa-plus-square"></i> ', ['create'], ['class' => 'icono iconoNuevo']) ?>
                    </td>
                </tr>
            <?php
            }
            ?>
        </tbody>
    </table>
</div>