<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Ordenadores $model */

$this->title = 'Actualizar ordenador: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ordenadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'ID: ' . $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="ordenadores-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>