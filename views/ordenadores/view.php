<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Ordenadores $model */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ordenadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ordenadores-view">



    <p>
        <?= Html::a('Actualizar <i class="fas fa-pencil-alt"></i>', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar <i class="fas fa-trash-alt"></i>', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro que quieres eliminar el registro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'descripcion',
            'procesador',
            'memoria',
            'discoDuro',
            //'ethernet',
            [
                'attribute' => 'ethernet',
                'format' => 'raw',
                'value' => function ($dato) {
                    return    $dato->ethernet  ? '<i class="fas fa-check-square"></i>' : '<i class="far fa-square"></i>';
                }
            ],
            [
                'attribute' => 'wifi',
                'format' => 'raw',
                'value' => function ($dato) {
                    return    $dato->wifi ? '<i class="fas fa-check-square"></i>' : '<i class="far fa-square"></i>';
                }
            ],
            //'wifi:boolean',
            'tarjetaVideo',
        ],
    ]) ?>

</div>