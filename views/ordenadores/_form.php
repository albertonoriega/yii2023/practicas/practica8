<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Ordenadores $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="ordenadores-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'procesador')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'memoria')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'discoDuro')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ethernet')->checkbox() ?>

    <?= $form->field($model, 'wifi')->checkbox() ?>

    <?= $form->field($model, 'tarjetaVideo')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>