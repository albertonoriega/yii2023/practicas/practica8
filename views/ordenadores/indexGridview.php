<?php

use app\models\Ordenadores;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Ordenadores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ordenadores-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div>
        <p>
            <?= Html::a('<i class="fas fa-plus-square"></i> <i class="fas fa-laptop"></i>', ['create'], ['class' => 'btn btn-info']) ?>
        </p>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'descripcion',
                'procesador',
                'memoria',
                'discoDuro',
                //'ethernet:boolean',
                //'wifi:boolean',
                [
                    'attribute' => 'ethernet',
                    'format' => 'raw',
                    'value' => function ($dato) {
                        return    $dato->ethernet ? '<i class="fas fa-check-square"></i>' : '<i class="far fa-square"></i>';
                    }
                ],
                [
                    'attribute' => 'wifi',
                    'format' => 'raw',

                    'value' => function ($dato) {
                        return    $dato->wifi  ? '<i class="fas fa-check-square"></i>' : '<i class="far fa-square"></i>';
                    }
                ],
                'tarjetaVideo',
                [
                    'class' => ActionColumn::className(),
                    'urlCreator' => function ($action, Ordenadores $model, $key, $index, $column) {
                        return Url::toRoute([$action, 'id' => $model->id]);
                    }
                ],

            ],
        ]); ?>

    </div>
</div>