<?php

use yii\helpers\Html;
?>

<div class="row">
    <?php

    foreach ($datos as $dato) {
    ?>
        <div class="col-4 mb-3">
            <div class="card cartas">
                <div class="tituloCard">
                    <div class="id"> <?= $dato->id ?></div>
                </div>
                <div class="card-body">
                    <h5 class="card-title"><?= $dato->descripcion ?></h5>
                </div>
                <div class="card-body">
                    <?= Html::a('Mas detalles', ['ordenadores/view', 'id' => $dato->id], ['class' => 'btn btn-light']) ?>
                </div>
            </div>
        </div>
    <?php
    }
    ?>
</div>