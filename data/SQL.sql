﻿DROP DATABASE IF EXISTS practica8Yii2023;

CREATE DATABASE practica8Yii2023;

USE practica8Yii2023;

CREATE TABLE ordenadores (
id int AUTO_INCREMENT,
descripcion varchar(255),
procesador varchar(100),
memoria varchar(100),
discoDuro varchar(100),
ethernet boolean,
wifi boolean,
tarjetaVideo varchar(100),
PRIMARY KEY (id)
);

INSERT INTO ordenadores ( descripcion, procesador, memoria, discoDuro, ethernet, wifi, tarjetaVideo)
  VALUES ('Ordenador de sobremesa HP', 'Core 2 Duo', 'DDR3 8GB', '250 GB SSD', FALSE, FALSE, 'Integrada'); 